extends Control
onready var cards: Array = [$Card0,$Card1,$Card2,$Card3]
func _on_Hand_card_change(value):
	var i = 0
	if value.size()==1:
		cards[0].hide()
		i+=1
	for card in value:
		cards[i].get_node("Name").text = card.name
		cards[i].get_node("Icon").texture = card.icon
		cards[i].get_node("Cost").text = str(card.cost)
		cards[i].get_node("Text").text = card.text
		cards[i].get_node("Flavor").text = card.flavor
		cards[i].show()
		i+=1
	while i <4:
		cards[i].hide()
		i+=1
