extends Entity
var ignorescroll = false
signal hp_changed(new_hp)
signal mp_changed(new_mp)
signal cd_changed(new_cd)
func set_hp(value):
	.set_hp(value)
	emit_signal("hp_changed",value)
func set_mp(value):
	.set_mp(value)
	emit_signal("mp_changed",value)
func set_cd(value):
	.set_cd(value)
	emit_signal("cd_changed",value)
func _physics_process(delta):
	._physics_process(delta)
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	if input_vector!=Vector2.ZERO:
		animationTree.set("parameters/Idle/blend_position",input_vector)
		animationTree.set("parameters/Run/blend_position",input_vector)
		animationState.travel("Run")
		velocity = velocity.move_toward(input_vector*MAX_SPEED,ACCELERATION*delta)
	else:
		animationState.travel("Idle")
		velocity=velocity.move_toward(Vector2.ZERO,delta * FRICTION)
	velocity = move_and_slide(velocity)
func _unhandled_input(event):
   # Mouse in viewport coordinates.
	if event.is_action_pressed("left_click"):
		var active_card = $CardSystem/Hand.get_active_card()
		cast(active_card)
	if event.is_action("scroll_down"):
		if !ignorescroll:
			$CardSystem/Hand.index+=1
		ignorescroll=!ignorescroll
	if event.is_action("scroll_up"):
		if !ignorescroll:
			$CardSystem/Hand.index-=1
		ignorescroll=!ignorescroll
func cast(ability):
	if .cast(ability):
		#var scene = load("res://cards/Bullet.tscn")
		#var bullet = scene.instance()
		#bullet.position=self.position
		#bullet.target= get_global_mouse_position()
		#bullet.modulate=ability.color
		#get_tree().get_root().get_node("Game").add_child(bullet)
		#var effect = AuraEffect.new()
		#effect.position=self.position
		#add_child(effect)
		ability.get_instance().execute(self,ability.args)
		$CardSystem/Hand.remove_card()
func _ready():
	set_cd(cooldown)
