extends KinematicBody2D
class_name Entity
#MOVEMENT STUFF
const ACCELERATION = 500
const MAX_SPEED = 400
const FRICTION = 2000
var velocity = Vector2.ZERO
#ANIMATION STUFF
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
#BASE STATS
var hp : float = 10 setget set_hp
var max_hp : float = 100 setget set_max_hp
var hp_regen : float = 1
var mp : float = 10 setget set_mp
var mp_regen : float= 0.2
var max_mp  : float= 10 setget set_max_mp
var atk : float = 1
var def : float = 0
var cooldown : float = 2 setget set_cd

func set_hp(value):
	hp=clamp(value,0,max_hp)
	#TODO: handle death, maybe in subclasses?
func set_max_hp(value):
	max_hp=value
	#clamp hp to new max_hp
	set_hp(hp)
func set_mp(value):
	mp=clamp(value,0,max_mp)
func set_max_mp(value):
	max_mp=value
	#clamp hp to new max_mp
	set_mp(mp)
func set_cd(value):
	cooldown=value
func damaged(value):
	set_hp(hp-value+def)
func _physics_process(delta):
	set_hp(hp+hp_regen*delta)
	set_mp(mp+mp_regen*delta)
func cast(ability):
	if (ability != null and $CastTimer.is_stopped() and ability.cost<=mp):
		$CastTimer.wait_time=cooldown
		$CastTimer.start()
		set_mp(mp-ability.cost)
		return true
	else:
		return false


func _on_Hurtbox_area_entered(_area):
	print("HIT")
	queue_free()
