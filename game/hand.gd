extends Node

class_name Hand

export var cards : Array
export var index : int = 0 setget set_index
signal card_change(value)
func set_index(val=0):
	if cards.size()>0:
		if val <0:
			val = cards.size()-1
		index=val%cards.size()
	emit_signal("card_change",	get_card_window(index))
func get_active_card():
	return cards[index] if !cards.empty()  else null
func add_card(card):
	#remove card
	cards.append_array(card)
	#signal change
	emit_signal("card_change",	get_card_window(index))
func remove_card(at=index):
	#remove card
	cards.remove(at)
	#update index
	set_index(index)
func get_card_window(at):
	var window = []
	var i = 0
	while i<4 and i< cards.size():
		window.append(cards[(at-1+i)%cards.size()])
		i+=1
	return window
func _ready():
	call_deferred("set_index")
