extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.

func _on_Player_hp_changed(new_hp):
	$HpBar.value=new_hp


func _on_Player_mp_changed(new_mp):
	$MpBar.value=new_mp
func _physics_process(_delta):
	pass
	$CdBar.value=$CdBar.max_value-get_tree().get_root().get_node("Game/YSort/Player/CastTimer").time_left
func _on_Player_cd_changed(new_cd):
	$CdBar.max_value=new_cd
