extends MenuButton


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum DebugChoice{
	DRAW = 0
	SPAWN_ENEMY = 1
}
signal debug_draw
# Called when the node enters the scene tree for the first time.
func _ready():
	if get_popup().connect("id_pressed",self,"func_name") !=OK: print("ERROR")
func func_name( ID ):
	match ID:
		DebugChoice.DRAW:
			emit_signal("debug_draw")
		DebugChoice.SPAWN_ENEMY:
			var enemy=load("res://game/enemies/Bat.tscn").instance()
			enemy.position = Vector2(300,300)
			get_tree().get_root().get_node("Game/YSort").add_child(enemy)
		_:
			print("songa")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
