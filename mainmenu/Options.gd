extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	$ColorRect/VBoxContainer/BackButton.grab_focus()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BackButton_pressed():
	get_tree().change_scene("res://mainmenu/MainMenu.tscn")
