extends NinePatchRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity = Vector2.ZERO
var i = 0
onready var y = rect_position.y
# Called when the node enters the scene tree for the first time.
func _physics_process(delta):
	i+=delta*4
	rect_position.y=y+cos(i)*5
