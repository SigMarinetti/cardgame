extends Effect

class_name ShotEffect
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var scene = load("res://cards/Bullet.tscn")
func execute(s, args:Dictionary={}):
	.execute(s, args)
	var bullets: Array
	var waves = 1
	if (args.has("waves")):
		waves = args["waves"]
	for wave in waves:
		if (args.has("count")):
			for i in args["count"]:
				bullets.append(scene.instance())
				bullets[i].position=user.position+Vector2(rand_range(-340,340),rand_range(-340,340))
				bullets[i].target= get_global_mouse_position()+Vector2(rand_range(-140,140),rand_range(-140,140))
		else:
			bullets.append(scene.instance())
			bullets[0].position=user.position
			bullets[0].target= get_global_mouse_position()
		if(args.has("speed")):
			for bullet in bullets:
				bullet.speed_mult = args["speed"]
		if(args.has("color")):
			for bullet in bullets:
				bullet.modulate=args["color"]
		for bullet in bullets:
			get_tree().get_root().get_node("Game").add_child(bullet)
		yield(get_tree().create_timer(1.0), "timeout")
		bullets.clear()
	queue_free()
