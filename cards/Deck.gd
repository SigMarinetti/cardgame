extends Node
class_name Deck
export var CardList = ["cards/ObamaUltimateFlash.tres"]
func shuffle():
	CardList.shuffle()
func draw(x):
	var buffer = []
	for i in x:
		buffer.append(load(CardList.pop_front()))
	return buffer
func add(card,shuffle = true):
	CardList.append(card)
	if shuffle: shuffle()
func add_at(card,position):
	CardList.insert(position,card)
func _ready():
	shuffle()
