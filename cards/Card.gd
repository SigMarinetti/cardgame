extends Resource

class_name Card

export var name : String = "Card Name"
export var icon : Texture;
export var text : String = ""
export var flavor : String = ""
export var cost : int
export var args : Dictionary
export var effect_path : String
const effect_directory = "res://cards"
func get_instance():
	var instance = null
	var effect = load(effect_directory+"/"+effect_path)
	match effect_path.get_extension():
		"gd": instance=effect.new()
		"tscn": instance=effect.instance()
		var extension: print(extension)	
	return instance
