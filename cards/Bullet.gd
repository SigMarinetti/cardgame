extends KinematicBody2D

var target = Vector2.ZERO
var velocity = Vector2.ZERO
var direction = Vector2.ZERO
var speed_mult = 1
const ACCELERATION = 50
const MAX_SPEED = 400
const FRICTION = 200
onready var animationPlayer = $AnimationPlayer
func _ready():
	animationPlayer.play("Bullet")
func _physics_process(delta):
	velocity = velocity.move_toward((target-position).normalized()*MAX_SPEED*speed_mult,ACCELERATION*delta*speed_mult)
	if move_and_collide(velocity):
		end()
	if position.distance_to(target)<20:
		end()
func end():
	velocity = Vector2.ZERO
	target = position
	$CollisionShape2D.disabled = true
	animationPlayer.play("Detonate")

